//
//  Model.swift
//  testTaskIronWater
//
//  Created by Александр Волков on 18.03.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
class User: NSObject {
    
    var defaults = UserDefaults.standard
    
    var firstName: String? {
        get {
            return UserDefaults.standard.object(forKey: "firstName") as! String?
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "firstName")
            UserDefaults.standard.synchronize()
        }
    }

    var lastName: String? {
        get {
        return UserDefaults.standard.object(forKey: "lastName") as! String?
        }
        
        set {
        UserDefaults.standard.set(newValue, forKey: "lastName")
        UserDefaults.standard.synchronize()
        }
    }
    var fatherName: String? {
        get {
            return UserDefaults.standard.object(forKey: "fatherName") as! String?
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "fatherName")
            UserDefaults.standard.synchronize()
        }
    }
    var dateBirth: String? {
        get {
            return UserDefaults.standard.object(forKey: "dateBirth") as! String?
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "dateBirth")
            UserDefaults.standard.synchronize()
        }
    }

    var gender: String? {
        get {
            return UserDefaults.standard.object(forKey: "gender") as! String?
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "gender")
            UserDefaults.standard.synchronize()
        }
    }

    
    
    }




