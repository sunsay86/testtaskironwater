//
//  WriteTableViewController.swift
//  testTaskIronWater
//
//  Created by Александр Волков on 10.03.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class WriteTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var isChanged = false
    
    
    // MARK: model
    var oldUser: User! {
        didSet {
            newUser = oldUser ?? User()
        }
    }
    
    var newUser = User()
    var delegate: ReadTableViewController!
    
        // MARK: Set Navigation Bar
    
    func setNavBar() {
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "Редактирование"
        
        let saveButton = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(save(sender:)))
        
        self.navigationItem.setRightBarButton(saveButton, animated: true)
        
        let cancelButton = UIBarButtonItem(title: "< Назад", style: .plain, target: self, action: #selector(cancel(sender:)))
        
        self.navigationItem.setLeftBarButton(cancelButton, animated: true)
        
    }
    
   
    // MARK: save
    func isValid(user: User!)->Bool {
        if  !user.firstName!.isEmpty &&
            !user.lastName!.isEmpty  &&
            !user.dateBirth!.isEmpty &&
            !user.gender!.isEmpty {
            
           
            return true
        }
        
        return false
        
        
    }
    
    func save(sender: UIBarButtonItem) {
       
        self.savedNewUser(user: newUser)
        
        if self.isValid(user: newUser) {
            delegate.saved(user: newUser)
            
            self.navigationController?.popToRootViewController(animated: true)
            
            
            
        } else {
            let alert = UIAlertController(title: "Ошибка!", message: "Все поля, кроме Отчество, являются обязательными, пол должен быть указан!", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: { (_) -> Void in
             
            })
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func savedNewUser(user: User!) {
        
        
            user.firstName = firstNameTextField.text!
            user.lastName = lastNameTextField.text!
            user.fatherName = fatherNameTextField.text!.isEmpty ? " " : fatherNameTextField.text!
            user.dateBirth = dateTextField.text!
            user.gender = genderTextField.text! == "не указан" ? "" : genderTextField.text!
    }
    
    // MARK: cancel
    
    func cancel(sender: UIBarButtonItem) {
       
            if !isChanged  {
                
                self.navigationController?.popToRootViewController(animated: true)
            
            } else {
                
                let alert = UIAlertController(title: "Данные были изменены", message: "Вы желаете сохранить изменения?", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Пропустить", style: .cancel, handler: { (_) -> Void in
                self.navigationController?.popToRootViewController(animated: true)
                })
                let saveAction = UIAlertAction(title: "Сохранить", style: .destructive, handler: { (_) -> Void in
                self.save(sender: sender)
                })
                alert.addAction(cancelAction)
                alert.addAction(saveAction)
                self.present(alert, animated: true, completion: nil)
            }
        
       
    }
    // MARK: Outlets
    
    @IBOutlet weak var firstNameTextField: UITextField! {
        didSet {
            firstNameTextField.delegate = self
        }
    }
    
    @IBOutlet weak var lastNameTextField: UITextField! {
        didSet {
            
            lastNameTextField.delegate = self
        }
    }
   
    @IBOutlet weak var fatherNameTextField: UITextField! {
        didSet {
            fatherNameTextField.delegate = self
        }
    }
        
    func updateUI() {
        
        if let user = oldUser {
            
            firstNameTextField.text = user.firstName
            lastNameTextField.text = user.lastName
            fatherNameTextField.text = user.fatherName
            dateTextField.text = user.dateBirth
            genderTextField.text = user.gender
        }
        
        
    }
    
    // MARK: date Text Field
    
    @IBOutlet weak var dateTextField: UITextField! {
        didSet {
            dateTextField.delegate = self
            
        }
    }
    
    func dateTextFieldEdit(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        dateTextField.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    // MARK: Gender Text Field
    var genderOption = ["мужской", "женский", "не указан"]
    
    @IBOutlet weak var genderTextField: UITextField! {
        didSet {
            genderTextField.delegate = self
        }
    }
    
    func genderTextFieldEdit(_ sender: UITextField) {
        let genderPickerView = UIPickerView()
        
        genderPickerView.delegate = self
        sender.inputView = genderPickerView
        
    }
    
    // MARK: Picker Data Source
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTextField.text = genderOption[row]
    }
    
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var nextField = UITextField()
        
        switch textField {
        case firstNameTextField : nextField = lastNameTextField
        case lastNameTextField: nextField = fatherNameTextField
        case fatherNameTextField: nextField = dateTextField
        default: break
        }
        
        nextField.becomeFirstResponder()
        return true
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.clearsOnBeginEditing = true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.isChanged = true
    }

    // MARK: LifeCycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.isChanged = false
    }
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavBar()
        self.dateTextFieldEdit(dateTextField)
        self.genderTextFieldEdit(genderTextField)
        updateUI()
    }
    
    
}
