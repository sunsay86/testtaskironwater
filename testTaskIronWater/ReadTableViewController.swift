//
//  ReadTableViewController.swift
//  testTaskIronWater
//
//  Created by Александр Волков on 10.03.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class ReadTableViewController: UITableViewController, ReadTVCDelegate {
    
    internal func saved(user: User?) {
        self.user = user!
        
    }

    // MARK: model
    var user = User()
    
    
    // MARK: Outlets
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var fatherNameLabel: UILabel!
    @IBOutlet weak var birthLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
   // MARK: set Navigation Bar
    
    func setNavBar(with buttonTitle: String ) {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "Просмотр"
        
        let editButton = UIBarButtonItem(title: buttonTitle, style: .plain, target: self, action: #selector(editButton(sender:)))
        
        self.navigationItem.setRightBarButton(editButton, animated: true)
        
    }
    
    func editButton(sender: UIBarButtonItem) {
        
        if let wtvc = ((UIStoryboard(name: "Main", bundle: nil)).instantiateViewController(withIdentifier: "WriteVC") as? WriteTableViewController) {
            self.navigationController?.pushViewController(wtvc, animated: true)
            wtvc.oldUser = self.user
            
            wtvc.delegate = self
        }
        
        
        
    }
    // MARK: update UI
    
    func updateUI() {
        
        firstNameLabel.text = user.firstName!
        lastNameLabel.text = user.lastName!
        fatherNameLabel.text = user.fatherName!
        birthLabel.text = user.dateBirth!
        genderLabel.text = user.gender!
        
    }
    
    // MARK: LifeCycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        if self.user != nil {
            setNavBar(with: "Редактировать")
            updateUI()
            
        } else {
            setNavBar(with: "Создать")
            
        }
        
        
    
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
   
}


protocol ReadTVCDelegate {
    func saved(user: User?)
    
}

